package mtt.ie.weatherforecast.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by Thiago Amanajás on 12/07/16.
 *
 * This class is responsible for detect the touch on
 * the item in the list.
 */
public class GeoAdapterClickListener implements RecyclerView.OnItemTouchListener {

    // An interface made for be used on the activity which contains the list
    private OnItemClickListener mListener;

    // A normal gesture detector
    private GestureDetector mGestureDetector;

    public interface OnItemClickListener {
        public void onPlaceClick(View view, int position);
    }

    /**
     * Initialize
     *
     * @param context
     * @param listener
     */
    public GeoAdapterClickListener(Context context, OnItemClickListener listener) {
        mListener = listener;
        mGestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View childView = rv.findChildViewUnder(e.getX(), e.getY());
        if (childView != null && mListener != null && mGestureDetector.onTouchEvent(e)) {
            mListener.onPlaceClick(childView, rv.getChildAdapterPosition(childView));
        }
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        // DO NOTHING
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        // DO NOTHING
    }
}
