package mtt.ie.weatherforecast.adapter;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import mtt.ie.weatherforecast.R;
import mtt.ie.weatherforecast.dao.GeoPlace;

/**
 * Created by Thiago Amanajás on 12/07/16.
 */
public class GeoPlaceAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    // Time waiting in the pending list
    private static final int PENDING_REMOVAL_TIMEOUT = 3000; // 3sec

    private List<GeoPlace> itemsPendingRemoval;
    private int lastInsertedIndex; // so we can add some more items for testing purposes
    private boolean undoOn; // is undo on, you can turn it on from the toolbar menu

    // hanlder for running delayed runnables
    private Handler handler = new Handler();
    // map of items to pending runnables, so we can cancel a removal if need be
    private HashMap<String, Runnable> pendingRunnables = new HashMap<>();

    private List<GeoPlace> mGeoPlaceList;
    private IGeoAdapterCallback mCallback;

    /**
     * Initialize
     * @param callback
     * @param list List of geoPlace to be listed
     */
    public GeoPlaceAdapter(IGeoAdapterCallback callback, List<GeoPlace> list) {
        mCallback = callback;
        mGeoPlaceList = list;
        itemsPendingRemoval = new ArrayList<>();
    }

    /**
     * Returns true if the item is about to be removed
     * @param position
     * @return
     */
    public boolean isPendingRemoval(int position) {
        GeoPlace item = mGeoPlaceList.get(position);
        return itemsPendingRemoval.contains(item);
    }

    /**
     * This method is used to be able to recovery the delete item
     * @param undoOn
     */
    public void setUndoOn(boolean undoOn) {
        this.undoOn = undoOn;
    }

    /**
     *
     * @return
     */
    public boolean isUndoOn() {
        return undoOn;
    }

    /**
     * Add a new geoplace in the list
     * and refresh the view
     * @param geoPlace
     */
    public void add(GeoPlace geoPlace) {
        mGeoPlaceList.add(geoPlace);
        Collections.sort(mGeoPlaceList, new Comparator<GeoPlace>() {
            @Override
            public int compare(GeoPlace lhs, GeoPlace rhs) {
                return lhs.getName().compareTo(rhs.getName());
            }
        });
        mCallback.addPlace(geoPlace);
        notifyDataSetChanged();
    }

    /**
     * Remove the item from the list
     * @param position
     */
    public void remove(int position) {

        GeoPlace item = mGeoPlaceList.get(position);
        if (itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.remove(position);
        }
        if (mGeoPlaceList.contains(item)) {
            mGeoPlaceList.remove(position);
            mCallback.removePlace(item);
            notifyItemRemoved(position);
        }
    }

    /**
     *
     * @param position
     */
    public void pendingRemoval(int position) {
        final GeoPlace item = mGeoPlaceList.get(position);
        if (!itemsPendingRemoval.contains(item)) {
            itemsPendingRemoval.add(item);
            // this will redraw row in "undo" state
            notifyItemChanged(position);
            // let's create, store and post a runnable to remove the item
            Runnable pendingRemovalRunnable = new Runnable() {
                @Override
                public void run() {
                    remove(mGeoPlaceList.indexOf(item));
                }
            };
            handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
            pendingRunnables.put(String.valueOf(item.getId()), pendingRemovalRunnable);
        }
    }

    /**
     *
     * @param position
     * @return
     */
    public GeoPlace getItem(int position) {
        return mGeoPlaceList != null && position >= 0 &&
                position < mGeoPlaceList.size() ? mGeoPlaceList.get(position) : null;
    }

    /**
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.geo_item, null);
        return new ItemPlaceHolder(v);
    }

    /**
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        ItemPlaceHolder viewHolder = (ItemPlaceHolder) holder;
        final GeoPlace item = mGeoPlaceList.get(position);

        if (itemsPendingRemoval.contains(item)) {
            // we need to show the "undo" state of the row
            viewHolder.itemView.setBackgroundColor(Color.RED);
            viewHolder.placeName.setVisibility(View.GONE);
            viewHolder.undoButton.setVisibility(View.VISIBLE);
            viewHolder.undoButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // user wants to undo the removal, let's cancel the pending task
                    Runnable pendingRemovalRunnable = pendingRunnables.get(item);
                    pendingRunnables.remove(item);
                    if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
                    itemsPendingRemoval.remove(item);
                    // this will rebind the row in "normal" state
                    notifyItemChanged(mGeoPlaceList.indexOf(item));
                }
            });
        } else {
            // we need to show the "normal" state
            viewHolder.itemView.setBackgroundColor(Color.WHITE);
            viewHolder.placeName.setVisibility(View.VISIBLE);
            viewHolder.placeName.setText(item.getName());
            viewHolder.undoButton.setVisibility(View.GONE);
            viewHolder.undoButton.setOnClickListener(null);
        }
    }

    /**
     *
     * @return
     */
    @Override
    public int getItemCount() {
        return mGeoPlaceList != null ? mGeoPlaceList.size() : 0;
    }

    /**
     *
     */
    public static class ItemPlaceHolder extends RecyclerView.ViewHolder {
        TextView placeName;
        Button undoButton;

        public ItemPlaceHolder(View itemView) {
            super(itemView);
            placeName = (TextView) itemView.findViewById(R.id.placeName);
            undoButton = (Button) itemView.findViewById(R.id.undo_button);
        }
    }
}
