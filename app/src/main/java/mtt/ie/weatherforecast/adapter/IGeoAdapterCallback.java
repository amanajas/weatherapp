package mtt.ie.weatherforecast.adapter;

import mtt.ie.weatherforecast.dao.GeoPlace;

/**
 * Created by Thiago Amanajás on 12/07/16.
 *
 * This class is used to call the activity
 * from inside the adapter
 */
public interface IGeoAdapterCallback {

    /**
     * Add in the database
     * @param place
     */
    void addPlace(GeoPlace place);

    /**
     * Remove from the database
     * @param place
     */
    void removePlace(GeoPlace place);
    // Open place picker
    void addNewPlace();
}
