package mtt.ie.weatherforecast.controller;

import android.app.Application;

/**
 * Created by Thiago Amanajás on 11/07/16.
 */
public class ApplicationController extends Application {

    /**
     * Log or request TAG
     */
    public static final String TAG = "ApplicationController";

    /**
     * A singleton instance of the application class for easy access in other places
     */
    private static ApplicationController sInstance;

    /**
     * A controller which allows database operations
     */
    private DatabaseController mDatabaseController;

    /**
     * The app preferences which allows the app
     * determine if it is the first access and
     * the key for the requests
     */
    private PreferenceController mPreferenceController;

    @Override
    public void onCreate() {
        super.onCreate();

        // initialize the singleton
        sInstance = this;

        // initialize the database
        mDatabaseController = new DatabaseController(this);

        // initialize preferences
        mPreferenceController = new PreferenceController(this);
    }

    /**
     *
     * @return PreferenceController instance
     */
    public PreferenceController getPreferenceController() {return mPreferenceController; }

    /**
     *
     * @return DatabaseController instance
     */
    public DatabaseController getDatabaseController() {
        return mDatabaseController;
    }

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized ApplicationController getInstance() {
        return sInstance;
    }

}