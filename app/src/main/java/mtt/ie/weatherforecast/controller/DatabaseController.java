package mtt.ie.weatherforecast.controller;

import android.content.Context;

import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;
import mtt.ie.weatherforecast.dao.DaoMaster;
import mtt.ie.weatherforecast.dao.DaoSession;
import mtt.ie.weatherforecast.dao.GeoPlace;
import mtt.ie.weatherforecast.dao.GeoPlaceDao;

/**
 * Created by Thiago Amanajás on 12/07/16.
 *
 * This class is an extension of the package DAO, created to
 * facilitated the use of the dao classes
 */
public class DatabaseController {

    // Helper database object
    private DaoMaster.DevOpenHelper mHelper;

    /**
     * Initialize
     * @param context
     */
    public DatabaseController(final Context context) {
        mHelper = new DaoMaster.DevOpenHelper(context,
                "forecastplaces_db", null);
    }

    /**
     * Returns a dao session
     * @return
     */
    private DaoSession getSession() {
        DaoMaster daoMaster = new DaoMaster(mHelper.getWritableDatabase());
        return daoMaster.newSession();
    }

    /**
     * Returns a size one list of geoplace
     * @param id
     * @return
     */
    public List<GeoPlace> getPlaces(long id) {
        DaoSession session = getSession();
        GeoPlaceDao dao = session.getGeoPlaceDao();
        QueryBuilder query = dao.queryBuilder().where(GeoPlaceDao.Properties.Id.eq(id));
        session.clear();
        return query.list();
    }

    /**
     * Return all geoplaces ordered by name
     * @return
     */
    public List<GeoPlace> getPlaces() {
        DaoSession session = getSession();
        GeoPlaceDao dao = session.getGeoPlaceDao();
        QueryBuilder query = dao.queryBuilder();
        query.orderAsc(GeoPlaceDao.Properties.Name);
        session.clear();
        return query.list();
    }

    /**
     * Insert a geoplace in the database
     * @param entity
     */
    public void insertPlace(GeoPlace... entity) {
        DaoSession session = getSession();
        GeoPlaceDao dao = session.getGeoPlaceDao();
        dao.insertOrReplaceInTx(entity);
        session.clear();
    }

    /**
     * Delete a geoplace
     * @param entity
     */
    public void deletePlace(GeoPlace... entity) {
        DaoSession session = getSession();
        GeoPlaceDao dao = session.getGeoPlaceDao();
        dao.deleteInTx(entity);
        session.clear();
    }

}
