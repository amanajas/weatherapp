package mtt.ie.weatherforecast.controller;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Thiago Amanajás on 13/07/16.
 */
public class PreferenceController {

    // Key used to get the shared preferences
    public static final String sPreferencesKey = "preferences";
    // Key used to get the first access value
    public static final String sFirstAccessKey = "first access";

    // Shared preferences object
    private SharedPreferences mShared;
    // Editor of the preferences
    private SharedPreferences.Editor mEditor;

    /**
     * Initialize
     * @param context
     */
    public PreferenceController(Context context) {
        mShared = context.getSharedPreferences(sPreferencesKey, Activity.MODE_PRIVATE);
        mEditor = mShared.edit();
    }

    /**
     * Set the first access value if the app
     * already was initialized
     * @param value
     */
    public void setFirstAccess(boolean value) {
        mEditor.putBoolean(sFirstAccessKey, value);
        mEditor.commit();
    }

    /**
     * Check if the app was opened
     * @return true if it is still the first time opening
     */
    public boolean isFirstAccess() {
        return mShared.getBoolean(sFirstAccessKey, true);
    }

}