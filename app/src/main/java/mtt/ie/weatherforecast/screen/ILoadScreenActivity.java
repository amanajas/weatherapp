package mtt.ie.weatherforecast.screen;

/**
 * Created by Thiago Amanajás on 13/07/16.
 *
 * The propose of this interface is to
 * assure that both of the activities have
 * these methods
 */
public interface ILoadScreenActivity {

    // To hide views
    void stopLoading();

    // To show views
    void startLoading();

}
