package mtt.ie.weatherforecast.screen;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;

import mtt.ie.weatherforecast.R;
import mtt.ie.weatherforecast.adapter.GeoAdapterClickListener;
import mtt.ie.weatherforecast.adapter.GeoPlaceAdapter;
import mtt.ie.weatherforecast.adapter.IGeoAdapterCallback;
import mtt.ie.weatherforecast.controller.ApplicationController;
import mtt.ie.weatherforecast.controller.PreferenceController;
import mtt.ie.weatherforecast.dao.GeoPlace;
import mtt.ie.weatherforecast.decorator.RecycleViewDecorator;
import mtt.ie.weatherforecast.listener.RecycleViewSimpleCallback;

public class MainActivity extends AppCompatActivity implements ILoadScreenActivity, IGeoAdapterCallback,
        GeoAdapterClickListener.OnItemClickListener {

    // Place picker limit
    public static final int sPlacePickerRequest = 1;

    // Place picker intent
    private PlacePicker.IntentBuilder mPlacePickerBuilder = new PlacePicker.IntentBuilder();

    // Adapter of geoplaces
    private GeoPlaceAdapter mGeoPlaceListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Setup Application content
        // Check if it is the first time starting
        PreferenceController pref = ApplicationController.getInstance().getPreferenceController();
        if (pref.isFirstAccess()) {

            String[] cities = getResources().getStringArray(R.array.start_app_cities);
            String[] latLonList = getResources().getStringArray(R.array.start_cities_coords);

            for (int i = 0; i < cities.length; i++) {

                GeoPlace geoPlace = new GeoPlace();
                geoPlace.setId((long) i);
                geoPlace.setName(cities[i]);
                String[] latLon = latLonList[i].split(",");

                geoPlace.setLat(Double.valueOf(latLon[0]));
                geoPlace.setLon(Double.valueOf(latLon[1]));

                // Add in the database
                ApplicationController.getInstance().getDatabaseController().insertPlace(geoPlace);

            }

            // Assure that these steps won't be repeated
            pref.setFirstAccess(false);
        }

        // Setting the floating button to pick places
        FloatingActionButton floatingActionButton = (FloatingActionButton) findViewById(R.id.addPlace);
        if (floatingActionButton != null) {
            floatingActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addNewPlace();
                }
            });
        }

        // Getting and configuring the list
        RecyclerView geoPlaceRecyclerView = (RecyclerView) findViewById(R.id.geoPlaceList);
        if (geoPlaceRecyclerView != null) {

            // Adding touch listener
            geoPlaceRecyclerView.addOnItemTouchListener(new GeoAdapterClickListener(this, this));

            LinearLayoutManager layoutManager = new LinearLayoutManager(this);
            geoPlaceRecyclerView.setLayoutManager(layoutManager);

            // Setup adapter
            mGeoPlaceListAdapter = new GeoPlaceAdapter(this, ApplicationController.getInstance()
                    .getDatabaseController().getPlaces());
            geoPlaceRecyclerView.setAdapter(mGeoPlaceListAdapter);
            geoPlaceRecyclerView.setHasFixedSize(true);

            // Enable to swipe items
            ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(new RecycleViewSimpleCallback(this,
                    mGeoPlaceListAdapter, 0, ItemTouchHelper.LEFT));
            mItemTouchHelper.attachToRecyclerView(geoPlaceRecyclerView);
            geoPlaceRecyclerView.addItemDecoration(new RecycleViewDecorator());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Stop loading when the activity loads
        stopLoading();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == sPlacePickerRequest) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, this);

                GeoPlace geoPlace = new GeoPlace();
                geoPlace.setName(place.getName().toString());
                geoPlace.setLat(place.getLatLng().latitude);
                geoPlace.setLon(place.getLatLng().longitude);

                mGeoPlaceListAdapter.add(geoPlace);
            }
        }
    }

    @Override
    public void addPlace(GeoPlace geoPlace) {
        Toast.makeText(this, R.string.new_place_added, Toast.LENGTH_LONG).show();
        ApplicationController.getInstance().getDatabaseController().insertPlace(geoPlace);
    }

    @Override
    public void removePlace(GeoPlace geoPlace) {
        Toast.makeText(this, R.string.place_removed, Toast.LENGTH_LONG).show();
        ApplicationController.getInstance().getDatabaseController().deletePlace(geoPlace);
    }

    @Override
    public void stopLoading() {


        RelativeLayout mLoadingComponent = (RelativeLayout) findViewById(R.id.loadingComponent);
        FloatingActionButton mFloatingActionButton = (FloatingActionButton)
                findViewById(R.id.addPlace);

        if (mLoadingComponent != null) {
            mLoadingComponent.setVisibility(View.GONE);
        }

        if (mFloatingActionButton != null) {
            mFloatingActionButton.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void startLoading() {


        RelativeLayout mLoadingComponent = (RelativeLayout) findViewById(R.id.loadingComponent);
        FloatingActionButton mFloatingActionButton = (FloatingActionButton)
                findViewById(R.id.addPlace);

        if (mLoadingComponent != null) {
            mLoadingComponent.setVisibility(View.VISIBLE);
        }

        if (mFloatingActionButton != null) {
            mFloatingActionButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void addNewPlace() {

        Toast.makeText(this, R.string.opening_place_picker, Toast.LENGTH_LONG).show();
        startLoading();

        try {
            startActivityForResult(mPlacePickerBuilder.build(this), sPlacePickerRequest);

        } catch (GooglePlayServicesRepairableException e) {

            e.printStackTrace();

        } catch (GooglePlayServicesNotAvailableException e) {

            e.printStackTrace();
        }
    }

    @Override
    public void onPlaceClick(View view, int position) {

        GeoPlace geoPlace = mGeoPlaceListAdapter.getItem(position);

        if (geoPlace != null) {

            // Initiating weather activity
            Intent forecastIntent = new Intent(this, WeatherActivity.class);
            forecastIntent.putExtra(WeatherActivity.sGeoPlaceIdBundle, geoPlace.getId());
            startActivity(forecastIntent);
        }

    }
}
