package mtt.ie.weatherforecast.screen;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.survivingwithandroid.weather.lib.WeatherClient;
import com.survivingwithandroid.weather.lib.WeatherConfig;
import com.survivingwithandroid.weather.lib.exception.WeatherLibException;
import com.survivingwithandroid.weather.lib.exception.WeatherProviderInstantiationException;
import com.survivingwithandroid.weather.lib.model.CurrentWeather;
import com.survivingwithandroid.weather.lib.model.DayForecast;
import com.survivingwithandroid.weather.lib.model.WeatherForecast;
import com.survivingwithandroid.weather.lib.provider.forecastio.ForecastIOProviderType;
import com.survivingwithandroid.weather.lib.request.WeatherRequest;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import mtt.ie.weatherforecast.R;
import mtt.ie.weatherforecast.controller.ApplicationController;
import mtt.ie.weatherforecast.dao.GeoPlace;
import mtt.ie.weatherforecast.wrapper.WeatherIconMapper;

public class WeatherActivity extends AppCompatActivity implements ILoadScreenActivity,
        WeatherClient.WeatherEventListener, WeatherClient.ForecastWeatherEventListener {


    // Bundle key to get the geoPlace id;
    public static final String sGeoPlaceIdBundle = "geo weather id";

    // Date format used in the method showinfo()
    public final static SimpleDateFormat sDateFormat = new SimpleDateFormat("dd/MMM");

    // Geoplace id passed by bundle from MainActivity
    private long mGeoPlaceId;
    // Object used to receive the forecasts
    private WeatherForecast mForecast;
    // Object which contains current weather information
    private CurrentWeather mCurrentWeather;

    // Place retrieved from the database
    private GeoPlace mGeoPlace;

    // Request flag used to stop multiple requests
    private boolean mRequestFlag;

    // Request builder
    private WeatherClient.ClientBuilder mBuilder;

    // Request builder
    private WeatherConfig mRequestConfig;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forecast);

        ImageButton refresh = (ImageButton) findViewById(R.id.refreshButton);
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestForecast();
            }
        });
        mRequestFlag = false;
        mGeoPlaceId = this.getIntent().getLongExtra(sGeoPlaceIdBundle, -1);
        if (savedInstanceState != null) {
            mGeoPlaceId = savedInstanceState.getLong(sGeoPlaceIdBundle);
        }

        if (mGeoPlaceId > -1) {
            requestForecast();
        } else {
            requestFail(getString(R.string.forecast_not_found));
        }
    }

    /**
     * Start a weather request
     */
    private void requestForecast() {

        // Checking to block second time request sequentially
        if (!mRequestFlag) {
            mRequestFlag = true;
            startLoading();

            // Getting geo place
            List<GeoPlace> geoPlace = ApplicationController.getInstance().
                    getDatabaseController().getPlaces(mGeoPlaceId);
            mGeoPlace = geoPlace.get(0);

            // Building config
            mBuilder = new WeatherClient.ClientBuilder();
            mRequestConfig = new WeatherConfig();
            mRequestConfig.ApiKey = this.getString(R.string.DK_API_KEY);

            /**
             * Requesting weather
             *
             * It is not a perfect solution
             * but it is need to avoid too many
             * interfaces passing by param
             */
            new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... params) {

                    try {
                        WeatherClient client = mBuilder.attach(WeatherActivity.this)
                                .provider(new ForecastIOProviderType())
                                .httpClient(com.survivingwithandroid.weather.lib.StandardHttpClient.class)
                                .config(mRequestConfig)
                                .build();

                        WeatherRequest req = new WeatherRequest(mGeoPlace.getLon(),
                                mGeoPlace.getLat());


                        client.getCurrentCondition(req, WeatherActivity.this);

                    } catch (WeatherProviderInstantiationException e) {
                        e.printStackTrace();
                        requestFail(e.getMessage());
                    }

                    return null;
                }
            }.execute();
        }
    }

    @Override
    public void startLoading() {
        ProgressBar progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        ScrollView scrollView = (ScrollView) findViewById(R.id.container);
        scrollView.setVisibility(View.GONE);
    }

    @Override
    public void stopLoading() {
        mRequestFlag = false;
        ProgressBar progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        ScrollView scrollView = (ScrollView) findViewById(R.id.container);
        scrollView.setVisibility(View.VISIBLE);
    }

    /**
     * Show all the information about weather
     * using the current and forecast objects
     *
     */
    private void showInfo() {

        LinearLayout container = (LinearLayout)findViewById(R.id.forecastsInfo);
        if (container != null) {
            container.removeAllViews();
        }

        TextView summary = (TextView) findViewById(R.id.summary);
        TextView time = (TextView) findViewById(R.id.forecastTime);
        TextView tittle = (TextView) findViewById(R.id.forecastTitle);
        TextView temp = (TextView) findViewById(R.id.temperature);
        ImageView forecastImage = (ImageView) findViewById(R.id.forecastSummary);

        if (tittle != null) tittle.setText(mGeoPlace.getName());
        if (temp != null) {
            String temperature = String.valueOf(mCurrentWeather.weather.temperature.getTemp()) +
                    mCurrentWeather.getUnit().tempUnit;
            temp.setText(temperature);
        }
        /**
         * Here there is a date generator because
         * the current weather and the forecast
         * don't have the time sequence. It's expected
         * that the lib get the dates from today forward,
         * as this generator does.
         */
        Date date = new Date();
        Calendar gc =  new GregorianCalendar();
        gc.setTime(date);
        gc.add(GregorianCalendar.DAY_OF_MONTH, 0);

        if (time != null) time.setText(sDateFormat.format(gc.getTime()));

        if (summary != null)
            summary.setText(mCurrentWeather.weather.currentCondition.getDescr());

        if (forecastImage != null) {
            forecastImage.setImageResource(
                    WeatherIconMapper.getWeatherResource(mCurrentWeather.weather
                                    .currentCondition.getIcon(),
                            mCurrentWeather.weather.currentCondition.getWeatherId()));
        }
        List<DayForecast> forecastList = mForecast.getForecast();
        for (DayForecast dayForecast : forecastList) {

            View child = getLayoutInflater().inflate(R.layout.info_forecast, null);

            forecastImage = (ImageView) child.findViewById(R.id.forecastSummary);
            TextView minTemp = (TextView) child.findViewById(R.id.minTemperature);
            TextView maxTemp = (TextView) child.findViewById(R.id.maxTemperature);
            summary = (TextView) child.findViewById(R.id.summary);
            time = (TextView) child.findViewById(R.id.forecastTime);

            gc.add(GregorianCalendar.DAY_OF_MONTH, 1);
            time.setText(sDateFormat.format(gc.getTime()));
            minTemp.setText(String.valueOf(dayForecast.weather.temperature.getMinTemp()));
            maxTemp.setText(String.valueOf(dayForecast.weather.temperature.getMaxTemp()));
            summary.setText(dayForecast.weather.currentCondition.getDescr());

            forecastImage.setImageResource(
                    WeatherIconMapper.getWeatherResource(dayForecast.weather
                            .currentCondition.getIcon(),
                            dayForecast.weather.currentCondition.getWeatherId()));

            if (container != null) {
                container.addView(child);
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putLong(sGeoPlaceIdBundle, mGeoPlaceId);
    }

    /**
     * Used as a callback when the request fails
     * @param failResult
     */
    private void requestFail(String failResult) {
        Toast.makeText(this, failResult, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onWeatherRetrieved(WeatherForecast forecast) {
        // Save the forecast
        mForecast = forecast;

        /**
         * Run on UI Thread
         *
         * It is necessary to change
         * the views running in the UI thread
         * from another separated thread
         */
        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                // Check if both objects were received
                if (mForecast != null && mCurrentWeather != null) {
                    stopLoading();
                    showInfo();
                } else {
                    // Set button request off
                    mRequestFlag = false;
                    // Otherwise initiates the request again
                    requestForecast();
                }
            }
        });
    }

    /**
     * Retrieves current weather information
     * and executes another task to retrieve
     * forecast information
     * @param weather
     */
    @Override
    public void onWeatherRetrieved(CurrentWeather weather) {
        // Saving the weather info
        mCurrentWeather = weather;

        /**
         * Requesting forecast
         *
         * It is not a perfect solution
         * but it is need to avoid too many
         * interfaces passing by param
         */
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                try{
                    WeatherClient client = mBuilder.attach(WeatherActivity.this)
                            .provider(new ForecastIOProviderType())
                            .httpClient(com.survivingwithandroid.weather.lib.StandardHttpClient.class)
                            .config(mRequestConfig)
                            .build();

                    WeatherRequest req = new WeatherRequest(mGeoPlace.getLon(),
                            mGeoPlace.getLat());

                    client.getForecastWeather(req, WeatherActivity.this);

                } catch (WeatherProviderInstantiationException e) {
                    e.printStackTrace();
                    requestFail(e.getMessage());
                }
                return null;
            }
        }.execute();
    }

    @Override
    public void onWeatherError(WeatherLibException wle) {
        wle.printStackTrace();
        Toast.makeText(this, R.string.forecast_result_fail, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onConnectionError(Throwable t) {
        t.printStackTrace();
        Toast.makeText(this, R.string.connection_fail, Toast.LENGTH_SHORT).show();

    }
}
